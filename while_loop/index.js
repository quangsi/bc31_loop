// while(điều kiện){
//     hành động
// })

// điều kiện lặp

// vòng lặp While sẽ kiểm tra điều kiện lặp trước . Nếu true thì thực thi khối lệnh, nếu false thì kết thúc vòng lặp
var index = 3;
while (index > 0) {
  index--;
  //   0
  console.log("index", index);
  console.log("hello");
}

console.log("out");

var number = Math.floor(5.9);
// number =5
var inKetQua = function () {
  var numberValue = document.getElementById("txt-number").value * 1;

  console.log({ numberValue });

  var count = 0;
  var ketQua = numberValue / 2;
  while (ketQua >= 1) {
    count++;

    var content = ` Count : ${count}- Num : ${ketQua}`;
    console.log(content);

    ketQua = Math.floor(ketQua / 2);
  }

  /**
   *
   *
   * Input 8
   *
   *
   * 1  -  4
   * 2  - 2
   * 3 - 1
   *
   */
};
