// for( khởi tạo ; điều kiện ; bước nhảy)
// {
//     hành động
// }

// khởi tạo : chỉ dc gọi 1 lần duy nhất trong suốt quá trình lặp

// bước nhảy sẽ tự động được gọi sau khi hành động kết thúc

for (var index = 0; index < 5; index++) {
  console.log(index);
}

//  dự đoán biến count

var count = 0;

for (count; count <= 10; count++) {
  console.log("Do nothing");
}

console.log(count);
